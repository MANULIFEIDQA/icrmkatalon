<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_13022020 1325</name>
   <tag></tag>
   <elementGuidId>728ae9f5-ab62-4f6d-9e6b-f1f4473bf5b3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='item22']/a[2]/span/nobr/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>colname</name>
      <type>Main</type>
      <value>createdon</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>13/02/2020 13:25</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ms-crm-IL-MenuItem-MoreInfoItem</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>13/02/2020 13:25</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;item22&quot;)/a[@class=&quot;ms-crm-IL-MenuItem-Anchor ms-crm-IL-MenuItem-Anchor-Hover&quot;]/span[1]/nobr[@class=&quot;ms-crm-IL-MenuItem-Title ms-crm-IL-MenuItem-Title-Hover&quot;]/div[@class=&quot;ms-crm-IL-MenuItem-MoreInfo ms-crm-IL-MenuItem-MoreInfo-Rest ms-crm-IL-MenuItem-MoreInfo ms-crm-IL-MenuItem-MoreInfo-Hover ms-crm-IL-MenuItem-MoreInfo ms-crm-IL-MenuItem-MoreInfo-Hover ms-crm-IL-MenuItem-MoreInfo ms-crm-IL-MenuItem-MoreInfo-Hover ms-crm-IL-MenuItem-MoreInfo ms-crm-IL-MenuItem-MoreInfo-Rest ms-crm-IL-MenuItem-MoreInfo ms-crm-IL-MenuItem-MoreInfo-Hover&quot;]/div[@class=&quot;ms-crm-IL-MenuItem-MoreInfoItem&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/New Case/Page_Case New Case/iframe_Assign (Filtered)_contentIFrame0</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='item22']/a[2]/span/nobr/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Processing'])[1]/following::div[16]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Real-time Processes'])[1]/following::div[18]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Look Up More Records'])[1]/preceding::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='13/02/2020 13:25']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//nobr/div/div</value>
   </webElementXpaths>
</WebElementEntity>
