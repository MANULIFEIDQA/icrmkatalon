#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Check the value of PPUKP
  I want to use this template for my feature file

  @tag1
  Scenario Outline: Check the value of PPUKP
    Given I already on home page
    When Page is already load
    Then I go to policies page
    Then I input <polnum> on search text
    Then I go to policy info page by click the polnum

    Examples: 
      | polnum |
      | 14099  |