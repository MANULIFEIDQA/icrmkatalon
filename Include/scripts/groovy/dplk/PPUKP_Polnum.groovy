package dplk
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import directQuery.GoesToPolicyInfoPage



class PPUKP_Polnum {
	
	GoesToPolicyInfoPage gp = new GoesToPolicyInfoPage();

	@Given("I already on home page")
	def I_already_on_home_page() {
		print "on Home Page"
	}

	@When("Page is already load")
	def Page_is_already_load() {
		print "home page already load"
	}
	
	@Then("I go to policies page")
	def I_go_to_policies_page() {
		gp.goesToPoliciesPage()
	}
	
	@Then("I input (.*) on search text")
	def I_input_polnum_on_search_text(String polnum) {
		gp.searchPolnum(polnum)
	}
	
	@Then("I go to policy info page by click the polnum")
	def I_go_to_policy_info_page_by_click_the_polnum() {
		gp.goesToPolicyInfo()
	}
}