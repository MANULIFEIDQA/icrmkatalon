import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable
import workflow.RunWorkflow as RunWorkflow

WebUI.delay(2)

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('Header/ddService'))

WebUI.click(findTestObject('Header/menuService'))

WebUI.click(findTestObject('Header/subMenuCases'))

WebUI.waitForPageLoad(10)

WebUI.waitForElementClickable(findTestObject('Ribbon/btnNewCase'), 10)

WebUI.waitForElementPresent(findTestObject('Ribbon/btnNewCase'), 10)

CustomKeywords.'base.BasePage.clickJSByElement'(findTestObject('Ribbon/btnNewCase'))

KeywordUtil.logInfo((('Creating new case with task subject is ' + TaskSubject) + ' and Policy Number is ') + Polnum)

WebUI.waitForPageLoad(10)

WebUI.delay(1)

WebUI.switchToFrame(findTestObject('New Case/contentIFrame1'), 2)

CustomKeywords.'base.BasePage.clickJSByElement'(findTestObject('New Case/txtPolnum'))

CustomKeywords.'base.BasePage.sendKeysJS'('mli_policy_ledit', Polnum)

CustomKeywords.'base.BasePage.clickJSByElement'(findTestObject('New Case/btnSearchPolnum'))

WebUI.delay(1)

CustomKeywords.'base.BasePage.sendKeysEnter'(findTestObject('New Case/policies'))

WebUI.delay(1)

CustomKeywords.'base.BasePage.clickJSByElement'(findTestObject('New Case/txtCustomer'))

CustomKeywords.'base.BasePage.sendKeysJS'('customerid_ledit', CustomerName)

CustomKeywords.'base.BasePage.clickJSByElement'(findTestObject('New Case/btnSearchCustomer'))

WebUI.delay(1)

CustomKeywords.'base.BasePage.sendKeysEnter'(findTestObject('New Case/customer'))

WebUI.delay(1)

CustomKeywords.'base.BasePage.clickJSByElement'(findTestObject('New Case/lblVerificationResult'))

WebUI.delay(1)

if (VerificationResult.toString().toLowerCase().equals('success')) {
    CustomKeywords.'base.BasePage.selectDropdDownByIndex'(findTestObject('New Case/dpdVerificationResult'), 1)
} else if (VerificationResult.toString().toLowerCase().equals('suspicious')) {
    CustomKeywords.'base.BasePage.selectDropdDownByIndex'(findTestObject('New Case/dpdVerificationResult'), 2)

    CustomKeywords.'base.BasePage.clickJSByElement'(findTestObject('Object Repository/New Case/txtSuspiciousReason'))

    CustomKeywords.'base.BasePage.sendKeysJS'('mli_suspiciousreason_i', 'Not Sure')
} else {
    CustomKeywords.'base.BasePage.selectDropdDownByIndex'(findTestObject('New Case/dpdVerificationResult'), 3)
}

CustomKeywords.'base.BasePage.clickJSByElement'(findTestObject('New Case/lblCaseCategory'))

WebUI.delay(1)

if (CaseCategory.toString().toLowerCase().equals('inquiry')) {
    CustomKeywords.'base.BasePage.selectDropdDownByIndex'(findTestObject('New Case/dpdCaseCategory'), 0)
} else if (CaseCategory.toString().toLowerCase().equals('request')) {
    CustomKeywords.'base.BasePage.selectDropdDownByIndex'(findTestObject('New Case/dpdCaseCategory'), 1)
} else if (CaseCategory.toString().toLowerCase().equals('complaint')) {
    CustomKeywords.'base.BasePage.selectDropdDownByIndex'(findTestObject('New Case/dpdCaseCategory'), 2)
}

CustomKeywords.'base.BasePage.clickJSByElement'(findTestObject('New Case/txtTaskSubject'))

CustomKeywords.'base.BasePage.sendKeysJS'('mli_tasksubjectid_ledit', TaskSubject)

CustomKeywords.'base.BasePage.clickJSByElement'(findTestObject('New Case/btnSeacrhTaskSubject'))

WebUI.delay(1)

CustomKeywords.'base.BasePage.sendKeysEnter'(findTestObject('New Case/taskSubject'))

WebUI.delay(1)

WebUI.switchToDefaultContent()

WebUI.waitForElementClickable(findTestObject('Ribbon/btnSave'), 5)

CustomKeywords.'base.BasePage.clickJSByElement'(findTestObject('Ribbon/btnSave'))

WebUI.delay(1)

KeywordUtil.logInfo(((('Case for task subject ' + TaskSubject) + ' and Policy Number ') + Polnum) + ' has sucessfully created')

Date d = new Date();

String folderName = d.toString().replace(":", "_").replace(" ", "_"); 

String pathSuccess = "Reports/ScreenshotSuccess/"+folderName

String pathFailed = "Reports/ScreenshotFailed/"+folderName

WebUI.takeScreenshot(+pathSuccess+"/Open - " +TaskSubject+".png")

WebUI.takeScreenshot()

println(pathSuccess)

println(pathFailed)

WebUI.delay(1)

println('worfklow Code = ' + WorkflowCode)

RunWorkflow.workflowCode = WorkflowCode

RunWorkflow.taskSubject = TaskSubject

