import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import workflow.RunWorkflow as RunWorkflow

WebUI.delay(2)

WebUI.switchToDefaultContent()

WebUI.click(findTestObject('Header/ddService'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Header/menuService'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Header/subMenuCases'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForPageLoad(30, FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementClickable(findTestObject('Ribbon/btnNewCase'), 20, FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Ribbon/btnNewCase'), 20, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'base.BasePage.clickJSByElement'(findTestObject('Ribbon/btnNewCase'))

KeywordUtil.logInfo((('Creating new case with task subject is ' + TaskSubject) + ' and Policy Number is ') + Polnum)

WebUI.waitForPageLoad(20, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.switchToFrame(findTestObject('New Case/contentIFrame1'), 10, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'base.BasePage.clickJSByElement'(findTestObject('New Case/txtPolnum'))

CustomKeywords.'base.BasePage.sendKeysJS'('mli_policy_ledit', Polnum)

CustomKeywords.'base.BasePage.clickJSByElement'(findTestObject('New Case/btnSearchPolnum'))

WebUI.delay(1)

CustomKeywords.'base.BasePage.sendKeysEnter'(findTestObject('New Case/policies'))

WebUI.delay(1)

CustomKeywords.'base.BasePage.clickJSByElement'(findTestObject('New Case/txtCustomer'))

CustomKeywords.'base.BasePage.sendKeysJS'('customerid_ledit', CustomerName)

CustomKeywords.'base.BasePage.clickJSByElement'(findTestObject('New Case/btnSearchCustomer'))

WebUI.delay(1)

CustomKeywords.'base.BasePage.sendKeysEnter'(findTestObject('New Case/customer'))

WebUI.delay(1)

CustomKeywords.'base.BasePage.clickJSByElement'(findTestObject('New Case/lblVerificationResult'))

WebUI.delay(1)

if (VerificationResult.toString().toLowerCase().equals('success')) {
    CustomKeywords.'base.BasePage.selectDropdDownByIndex'(findTestObject('New Case/dpdVerificationResult'), 1)
} else if (VerificationResult.toString().toLowerCase().equals('suspicious')) {
    CustomKeywords.'base.BasePage.selectDropdDownByIndex'(findTestObject('New Case/dpdVerificationResult'), 2)

    CustomKeywords.'base.BasePage.clickJSByElement'(findTestObject('Object Repository/New Case/txtSuspiciousReason'))

    CustomKeywords.'base.BasePage.sendKeysJS'('mli_suspiciousreason_i', 'Not Sure')
} else {
    CustomKeywords.'base.BasePage.selectDropdDownByIndex'(findTestObject('New Case/dpdVerificationResult'), 3)
}

CustomKeywords.'base.BasePage.clickJSByElement'(findTestObject('New Case/lblCaseCategory'))

WebUI.delay(1)

if (CaseCategory.toString().toLowerCase().equals('inquiry')) {
    CustomKeywords.'base.BasePage.selectDropdDownByIndex'(findTestObject('New Case/dpdCaseCategory'), 0)
} else if (CaseCategory.toString().toLowerCase().equals('request')) {
    CustomKeywords.'base.BasePage.selectDropdDownByIndex'(findTestObject('New Case/dpdCaseCategory'), 1)
} else if (CaseCategory.toString().toLowerCase().equals('complaint')) {
    CustomKeywords.'base.BasePage.selectDropdDownByIndex'(findTestObject('New Case/dpdCaseCategory'), 2)
}

CustomKeywords.'base.BasePage.clickJSByElement'(findTestObject('New Case/txtTaskSubject'))

CustomKeywords.'base.BasePage.sendKeysJS'('mli_tasksubjectid_ledit', TaskSubject)

CustomKeywords.'base.BasePage.clickJSByElement'(findTestObject('New Case/btnSeacrhTaskSubject'))

WebUI.delay(1)

CustomKeywords.'base.BasePage.sendKeysEnter'(findTestObject('New Case/taskSubject'))

WebUI.delay(1)

WebUI.switchToDefaultContent(FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementClickable(findTestObject('Ribbon/btnSave'), 10, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'base.BasePage.clickJSByElement'(findTestObject('Ribbon/btnSave'))

WebUI.delay(1)

KeywordUtil.logInfo(((('Case for task subject ' + TaskSubject) + ' and Policy Number ') + Polnum) + ' has sucessfully created')

String pathReport = RunConfiguration.getReportFolder()

//WebUI.takeScreenshot(+pathReport+"\\Open - " +TaskSubject+".png")

WebUI.takeScreenshot()

WebUI.delay(1)

RunWorkflow.workflowCode = WorkflowCode

RunWorkflow.taskSubject = TaskSubject

