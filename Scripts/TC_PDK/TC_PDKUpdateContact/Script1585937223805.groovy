import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import oracleIntegration.PDKOracle as PDKOracle
import sqlIntegration.PDKSQL as PDKSQL
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

WebUI.waitForPageLoad(10, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.switchToDefaultContent(FailureHandling.STOP_ON_FAILURE)

WebUI.switchToFrame(findTestObject('New Case/contentIFrame1'), 10, FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('New Case/lblCaseNumber'), 10, FailureHandling.STOP_ON_FAILURE)

caseNumber = WebUI.getText(findTestObject('Object Repository/New Case/lblCaseNumber'))

WebUI.switchToDefaultContent(FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementClickable(findTestObject('Ribbon/btnPDK'), 10, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'base.BasePage.clickJSByElement'(findTestObject('Ribbon/btnPDK'))

WebUI.switchToFrame(findTestObject('PDK/InlineDialog_Iframe'), 10, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'base.BasePage.selectDropdDownByIndex'(findTestObject('Object Repository/PDK/dpdChangeType'), 1)

WebUI.waitForElementClickable(findTestObject('PDK/btnOk'), 10, FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'base.BasePage.clickJSByElement'(findTestObject('PDK/btnOk'))

WebUI.switchToDefaultContent(FailureHandling.STOP_ON_FAILURE)

WebUI.switchToFrame(findTestObject('PDK/InlineDialog1_Iframe'), 10, FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementClickable(findTestObject('PDK/Update Contact/txtMobilePhn'), 10, FailureHandling.STOP_ON_FAILURE)

//CustomKeywords.'base.BasePage.sendKeys'(findTestObject('PDK/Update Contact/txtMobilePhn'), '081212121')

//CustomKeywords.'base.BasePage.sendKeys'(findTestObject('PDK/Update Contact/txtEmail'), 'a@a.com')

CustomKeywords.'base.BasePage.sendKeysJS'("inpMobilePhoneNo", "08986192929")

CustomKeywords.'base.BasePage.sendKeysJS'("inpHomePhoneAC", "21")

CustomKeywords.'base.BasePage.sendKeysJS'("inpHomePhonePN", "522766")

CustomKeywords.'base.BasePage.sendKeysJS'("inpHomePhoneExt", "1234")

CustomKeywords.'base.BasePage.sendKeysJS'("inpOtherPhoneAC", "22")

CustomKeywords.'base.BasePage.sendKeysJS'("inpOtherPhoneNo", "081990000071")

CustomKeywords.'base.BasePage.sendKeysJS'("inpOtherPhoneExt", "4321")

CustomKeywords.'base.BasePage.sendKeysJS'("inpFaxAC", "711")

CustomKeywords.'base.BasePage.sendKeysJS'("inpFaxNo", "0812345")

CustomKeywords.'base.BasePage.sendKeysJS'("inpEmail", "a@a.com")

WebUI.takeScreenshot(FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'base.BasePage.clickJSByElement'(findTestObject('PDK/btnOk'))

WebUI.waitForAlert(10, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

alertText = WebUI.getAlertText()

WebUI.acceptAlert(FailureHandling.STOP_ON_FAILURE)

oi = new PDKOracle()

si = new PDKSQL()

if (alertText.equals('Your transaction is being processed')) {
    WebUI.delay(7)

    if (si.checkStatusPDK(caseNumber).get(0).equals('858740030')) {
        if (oi.getCountError(si.checkStatusPDK(caseNumber).get(1)).size() == 0) {
            //PASS
            CustomKeywords.'customPDK.Report.reportPDK'('PASS', 'UPDATE CONTACT - SUCCESS', caseNumber)

            CustomKeywords.'asserts.BooleanAssert.isTrue'(true, 'Update Contact - Passed', FailureHandling.STOP_ON_FAILURE //Oracle Issue
                //SQL Issue
                //create address failed
                )
        } else {
            CustomKeywords.'customPDK.Report.reportPDK'('FAIL', 'UPDATE CONTACT - ORACLE ISSUE', caseNumber)

            CustomKeywords.'asserts.BooleanAssert.isFalse'(false, 'Update Contact - Oracle Issue', FailureHandling.STOP_ON_FAILURE)
        }
    } else {
        CustomKeywords.'customPDK.Report.reportPDK'('FAIL', 'UPDATE CONTACT - SQL ISSUE', caseNumber)

        CustomKeywords.'asserts.BooleanAssert.isFalse'(false, 'Update Contact - SQL Issue', FailureHandling.STOP_ON_FAILURE)
    }
} else {
    KeywordUtil.markFailed('UPDATE Contact FAILED')

    CustomKeywords.'asserts.BooleanAssert.isFalse'(false, 'Update Contact Failed', FailureHandling.STOP_ON_FAILURE)
}

