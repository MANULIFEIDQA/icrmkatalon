package directQuery

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import base.BasePage

public class GoesToPolicyInfoPage {

	@Keyword
	def goToPolicyInfo(String polnum){

		BasePage bp = new BasePage();

		WebUI.waitForPageLoad(5)

		WebUI.switchToDefaultContent()

		WebUI.waitForElementClickable(findTestObject('Object Repository/Header/ddService'), 5)

		bp.clickElement(findTestObject('Object Repository/Header/ddService'))

		bp.clickElement(findTestObject('Object Repository/Header/menuService'))

		bp.clickElement(findTestObject('Object Repository/Header/subMenuPolicies'))

		WebUI.waitForPageLoad(5)

		WebUI.delay(2)

		WebUI.switchToFrame(findTestObject('Object Repository/Policies/contentIFrame0'), 10)

		bp.sendKeysJS('crmGrid_findCriteria', polnum)

		bp.clickElement(findTestObject('Object Repository/Policies/btnSeacrh'))

		WebUI.delay(1)

		WebUI.doubleClick(findTestObject('Object Repository/Policies/tblPolicyName'))
	}
}
