package base
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.Keys
import org.openqa.selenium.NoSuchFrameException
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.Select
import org.openqa.selenium.support.ui.Wait
import org.openqa.selenium.support.ui.WebDriverWait

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords


class BasePage {

	@Keyword
	def getElement(By locator){
		WebElement element = null;
		try{
			WebDriver driver = DriverFactory.getWebDriver()
			Wait wait = new WebDriverWait(driver, 15);
			wait.until(ExpectedConditions.presenceOfElementLocated(locator));
			element = driver.findElement(locator);
			return element;
		} catch(Exception e){
			e.message();
		}
	}

	@Keyword
	def refreshBrowser() {
		KeywordUtil.logInfo("Refreshing")
		WebDriver webDriver = DriverFactory.getWebDriver()
		webDriver.navigate().refresh()
		KeywordUtil.markPassed("Refresh successfully")
	}

	@Keyword
	def clickElement(TestObject to) {
		try {
			WebElement element = WebUiBuiltInKeywords.findWebElement(to);
			KeywordUtil.logInfo("Clicking element")
			element.click()
			KeywordUtil.markPassed("Element has been clicked")
		} catch (WebElementNotFoundException e) {
			KeywordUtil.markFailed("Element not found")
		} catch (Exception e) {
			KeywordUtil.markFailed("Fail to click on element")
		}
	}

	@Keyword
	def clickJSById(String id) {
		try {
			WebDriver driver = DriverFactory.getWebDriver()
			JavascriptExecutor jse = ((driver) as JavascriptExecutor)
			String script = "arguments[0].click();"
			KeywordUtil.logInfo("Clicking element")
			jse.executeScript(script, id);
		} catch (WebElementNotFoundException e) {
			KeywordUtil.markFailed("Element JS not found")
		} catch (Exception e) {
			println e.message
			KeywordUtil.markFailed("Fail to click JS on element")
		}
	}

	@Keyword
	def getValue(TestObject to){
		try{
			WebDriver driver = DriverFactory.getWebDriver()
			JavascriptExecutor jse = ((driver) as JavascriptExecutor)
			WebElement element = WebUiBuiltInKeywords.findWebElement(to);
			//String script = "arguments[0].value;"
			//String result = jse.executeScript(script, element);
			String result = element.getText()
			return result.toString()
		} catch (WebElementNotFoundException e){
			KeywordUtil.markFailed("Element By JS not found")
		} catch (Exception e){
			println e.message
			KeywordUtil.markFailed("Fail to get value JS By element")
		}
	}

	@Keyword
	def clickJSByElement(TestObject to){
		try{
			WebDriver driver = DriverFactory.getWebDriver()
			JavascriptExecutor jse = ((driver) as JavascriptExecutor)
			WebElement element = WebUiBuiltInKeywords.findWebElement(to);
			String script = "arguments[0].click();"
			jse.executeScript(script, element);
		} catch (WebElementNotFoundException e){
			KeywordUtil.markFailed("Element By JS not found")
		} catch (Exception e){
			println e.message
			KeywordUtil.markFailed("Fail to click JS By element")
		}
	}

	@Keyword
	def sendKeys(TestObject to, String text){
		try {
			WebDriver driver = DriverFactory.getWebDriver()
			JavascriptExecutor jse = ((driver) as JavascriptExecutor)
			WebElement element = WebUiBuiltInKeywords.findWebElement(to);
			KeywordUtil.logInfo("Clicking element")
			element.sendKeys(text)
		} catch (WebElementNotFoundException e) {
			KeywordUtil.markFailed("Element "+to+ " not found")
		} catch (Exception e) {
			println e.message
			KeywordUtil.markFailed("Fail to send keys on element"+to)
		}
	}

	@Keyword
	def sendKeysJS(String id, String text){
		try {
			WebDriver driver = DriverFactory.getWebDriver()
			Wait wait = new WebDriverWait(driver, 15);
			JavascriptExecutor jse = ((driver) as JavascriptExecutor)
			String script = "document.getElementById('"+id+"').value= arguments[0];"
			jse.executeScript(script, text);
		} catch (WebElementNotFoundException e) {
			KeywordUtil.markFailed("Error when sendKeysJS on "+id)
		} catch (Exception e) {
			println e.message
			KeywordUtil.markFailed("Error when sendKeysJS on "+id)
		}
	}

	@Keyword
	def waitForAvailableFrameAndSwitchToIt(String id){
		try{
			WebDriver driver = DriverFactory.getWebDriver()
			Wait wait = new WebDriverWait(driver, 15);
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(id));
		} catch (NoSuchFrameException e){
			KeywordUtil.markFailed("Frame "+id+" not found")
		} catch (NoSuchFrameException e){
			KeywordUtil.markFailed("Fail switch to frame"+id)
		}
	}

	@Keyword
	def sendKeysEnter(TestObject to){
		try{
			WebDriver driver = DriverFactory.getWebDriver()
			WebElement element = WebUiBuiltInKeywords.findWebElement(to);
			element.sendKeys(Keys.ENTER)
		}catch (WebElementNotFoundException e) {
			KeywordUtil.markFailed("Error when enter on "+to)
		} catch (Exception e) {
			println e.message
			KeywordUtil.markFailed("Error when enter on "+to)
		}
	}

	@Keyword
	def selectDropdDownByIndex(TestObject to, int idx){
		try{
			WebDriver driver = DriverFactory.getWebDriver()
			WebElement element = WebUiBuiltInKeywords.findWebElement(to);
			Select select = new Select(element)
			select.selectByIndex(idx);
		}catch (WebElementNotFoundException e) {
			KeywordUtil.markFailed("Error when enter on "+to)
		} catch (Exception e) {
			println e.message
			KeywordUtil.markFailed("Error when enter on "+to)
		}
	}

	@Keyword
	def selectDropdDownByVisibleText(TestObject to, String text){
		try{
			WebDriver driver = DriverFactory.getWebDriver()
			WebElement element = WebUiBuiltInKeywords.findWebElement(to);
			Select select = new Select(element)
			select.selectByVisibleText(text);
		}catch (WebElementNotFoundException e) {
			KeywordUtil.markFailed("Error when enter on "+to)
		} catch (Exception e) {
			println e.message
			KeywordUtil.markFailed("Error when enter on "+to)
		}
	}

	/**
	 * Get all rows of HTML table
	 * @param table Katalon test object represent for HTML table
	 * @param outerTagName outer tag name of TR tag, usually is TBODY
	 * @return All rows inside HTML table
	 */
	@Keyword
	def List<WebElement> getHtmlTableRows(TestObject table, String outerTagName) {
		WebElement mailList = WebUiBuiltInKeywords.findWebElement(table)
		List<WebElement> selectedRows = mailList.findElements(By.xpath("./" + outerTagName + "/tr"))
		return selectedRows
	}
}