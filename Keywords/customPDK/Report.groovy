package customPDK

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import oracleIntegration.PDKOracle as PDKOracle
import sqlIntegration.PDKSQL as PDKSQL


public class Report {

	@Keyword
	static void reportPDK(String status, String title, String caseNumber){

		PDKOracle oi = new PDKOracle()
		PDKSQL si = new PDKSQL()

		String refNum = si.checkStatusPDK(caseNumber).get(1)

		if(status.toUpperCase().equals("FAIL")){
			KeywordUtil.markFailed(title.toUpperCase())
			KeywordUtil.markFailed("Case Number = "+caseNumber)
			KeywordUtil.markFailed("REF NUM = "+refNum)
			KeywordUtil.markFailed("Status SQL = "+si.checkStatusPDK(caseNumber).get(0))
			for(int i=0; i<oi.getCountError(refNum).size();i++){
				KeywordUtil.markFailed("Count Error in Oracle = "+oi.getCountError(refNum).get(i).get(0))
				KeywordUtil.markFailed("Log Type = "+oi.getCountError(refNum).get(i).get(1))
				KeywordUtil.markFailed("Log Desc = "+oi.getCountError(refNum).get(i).get(2))
				KeywordUtil.markFailed("Log Date = "+oi.getCountError(refNum).get(i).get(3))
			}
		}else{
			KeywordUtil.markPassed(title.toUpperCase())
			KeywordUtil.markPassed("Case Number = "+caseNumber)
			KeywordUtil.markPassed("REF NUM = "+refNum)
			KeywordUtil.markPassed("Status SQL = "+si.checkStatusPDK(caseNumber).get(0))

			for(int i=0; i<oi.getCountError(refNum).size();i++){
				KeywordUtil.markPassed("Count Error in Oracle = "+oi.getCountError(refNum).get(i).get(0))
				KeywordUtil.markPassed("Log Type = "+oi.getCountError(refNum).get(i).get(1))
				KeywordUtil.markPassed("Log Desc = "+oi.getCountError(refNum).get(i).get(2))
				KeywordUtil.markPassed("Log Date = "+oi.getCountError(refNum).get(i).get(3))
			}
		}
	}
}
