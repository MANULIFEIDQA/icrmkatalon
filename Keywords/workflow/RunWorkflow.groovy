package workflow

import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import base.BasePage
import internal.GlobalVariable

public class RunWorkflow {

	static BasePage bp = new BasePage()
	static String caseOwnerActual = null
	static String statusActual = null
	public static String workflowCode = null
	public static String taskSubject = null;

	public boolean saveAndRoute(String caseOwnerExcel, String creator){
		KeywordUtil.logInfo("Before Save and route for actual case owner is "+caseOwnerExcel)
		WebUI.delay(1)
		WebUI.switchToDefaultContent(FailureHandling.STOP_ON_FAILURE)
		WebUI.waitForElementClickable(findTestObject('Ribbon/btnSaveAndRoute'), 20, FailureHandling.STOP_ON_FAILURE)
		bp.clickJSByElement(findTestObject('Ribbon/btnSaveAndRoute'))
		WebUI.waitForAlert(15, FailureHandling.STOP_ON_FAILURE)
		WebUI.acceptAlert(FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(5)

		WebUI.waitForPageLoad(20, FailureHandling.STOP_ON_FAILURE)
		WebUI.switchToDefaultContent(FailureHandling.STOP_ON_FAILURE)
		WebUI.switchToFrame(findTestObject('Object Repository/New Case/contentIFrame1'), 20, FailureHandling.STOP_ON_FAILURE)
		//WebUI.waitForElementClickable(findTestObject('Object Repository/New Case/lblCaseOwner'), 5)
		caseOwnerActual = bp.getValue(findTestObject('Object Repository/New Case/lblCaseOwner'))
		statusActual = bp.getValue(findTestObject('Object Repository/New Case/lblStatus'))

		//refresh status and caseOwner
		if(statusActual.toLowerCase().equals("open") || statusActual.toLowerCase().equals("in progress")){
			//InProgress || Open
			int iterasi1 = 0;
			while (caseOwnerActual.toLowerCase().equals(creator.toLowerCase()) && iterasi1 < 5) {
				WebUI.waitForPageLoad(10,FailureHandling.STOP_ON_FAILURE)
				WebUI.switchToDefaultContent(FailureHandling.STOP_ON_FAILURE)
				WebUI.waitForElementClickable(findTestObject('Object Repository/Ribbon/btnRefresh'), 20, FailureHandling.STOP_ON_FAILURE)
				bp.clickJSByElement(findTestObject('Object Repository/Ribbon/btnRefresh'))
				WebUI.switchToFrame(findTestObject('Object Repository/New Case/contentIFrame1'), 10, FailureHandling.STOP_ON_FAILURE)
				caseOwnerActual = bp.getValue(findTestObject('Object Repository/New Case/lblCaseOwner'))
				statusActual = bp.getValue(findTestObject('Object Repository/New Case/lblStatus'))
				iterasi1++;
			}

			if(caseOwnerActual.toLowerCase().equals(caseOwnerExcel.toLowerCase()) && statusActual.toLowerCase().equals("in progress")){
				KeywordUtil.markPassed("Success save and route for case owner is "+caseOwnerActual+" and status is "+statusActual)
				//WebUI.takeScreenshot("Reports/In Progress - "+taskSubject+".png")
				WebUI.takeScreenshot(FailureHandling.STOP_ON_FAILURE)
				return true
			}else{
				KeywordUtil.markFailed("Failed save and route because case owner is "+caseOwnerActual+" and status is "+statusActual)
				//WebUI.takeScreenshot("Error - In Progress - "+taskSubject+".png")
				WebUI.takeScreenshot(FailureHandling.STOP_ON_FAILURE)
				return false
			}
		}else{
			//ReadyToResolved
			int iterasi2 = 0;
			while (!caseOwnerActual.toLowerCase().equals(creator.toLowerCase()) && iterasi2 < 5) {
				WebUI.waitForPageLoad(20, FailureHandling.STOP_ON_FAILURE)
				WebUI.switchToDefaultContent(FailureHandling.STOP_ON_FAILURE)
				WebUI.waitForElementClickable(findTestObject('Object Repository/Ribbon/btnRefresh'), 20, FailureHandling.STOP_ON_FAILURE)
				bp.clickJSByElement(findTestObject('Object Repository/Ribbon/btnRefresh'))
				WebUI.switchToFrame(findTestObject('Object Repository/New Case/contentIFrame1'), 10, FailureHandling.STOP_ON_FAILURE)
				caseOwnerActual = bp.getValue(findTestObject('Object Repository/New Case/lblCaseOwner'))
				statusActual = bp.getValue(findTestObject('Object Repository/New Case/lblStatus'))
				iterasi2++
			}

			if(caseOwnerActual.toLowerCase().equals(creator.toLowerCase()) && statusActual.toLowerCase().equals("ready to resolve")){
				KeywordUtil.markPassed("Success save and route for case owner is "+caseOwnerActual+" and status is "+statusActual)
				//WebUI.takeScreenshot("Ready to Resolve - "+taskSubject)
				WebUI.takeScreenshot(FailureHandling.STOP_ON_FAILURE)
				WebUI.delay(1)
				return true
			}else{
				KeywordUtil.markFailed("Failed save and route because case owner is "+caseOwnerActual+" and status is "+statusActual)
				//WebUI.takeScreenshot("Error - Ready to Resolve - "+taskSubject)
				WebUI.takeScreenshot(FailureHandling.STOP_ON_FAILURE)
				WebUI.delay(1)
				return false
			}
		}
	}

	public boolean resolved(String creator){
		println workflowCode
		KeywordUtil.logInfo("Before Resolved the case for creator is "+creator)
		WebUI.switchToDefaultContent(FailureHandling.STOP_ON_FAILURE)
		WebUI.switchToFrame(findTestObject('Object Repository/New Case/contentIFrame1'), 10, FailureHandling.STOP_ON_FAILURE)
		caseOwnerActual = bp.getValue(findTestObject('Object Repository/New Case/lblCaseOwner'))
		statusActual = bp.getValue(findTestObject('Object Repository/New Case/lblStatus'))
		int iterasi = 0;

		while(!statusActual.toLowerCase().equals("ready to resolve") && iterasi < 5){
			WebUI.switchToDefaultContent(FailureHandling.STOP_ON_FAILURE)
			bp.clickJSByElement(findTestObject('Object Repository/Ribbon/btnRefresh'))
			WebUI.switchToFrame(findTestObject('Object Repository/New Case/contentIFrame1'), 10, FailureHandling.STOP_ON_FAILURE)
			caseOwnerActual = bp.getValue(findTestObject('Object Repository/New Case/lblCaseOwner'))
			statusActual = bp.getValue(findTestObject('Object Repository/New Case/lblStatus'))
			iterasi++
		}

		if(caseOwnerActual.toLowerCase().equals(creator.toLowerCase()) && statusActual.toLowerCase().equals("ready to resolve")){
			WebUI.switchToDefaultContent(FailureHandling.STOP_ON_FAILURE)
			bp.clickElement(findTestObject('Object Repository/Ribbon/btnMoreCommands'))

			WebUI.waitForElementClickable(findTestObject('Object Repository/Ribbon/MoreCommands/btnRunWorkflow'), 10, FailureHandling.STOP_ON_FAILURE)
			bp.clickElement(findTestObject('Object Repository/Ribbon/MoreCommands/btnRunWorkflow'))

			WebUI.switchToFrame(findTestObject('Object Repository/Ribbon/MoreCommands/InlineDialog_Iframe'), 10, FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementClickable(findTestObject('Object Repository/Ribbon/MoreCommands/btnAdd1'), 10, FailureHandling.STOP_ON_FAILURE)
			bp.clickElement(findTestObject('Object Repository/Ribbon/MoreCommands/btnAdd1'))

			WebUI.switchToDefaultContent(FailureHandling.STOP_ON_FAILURE)
			WebUI.switchToFrame(findTestObject('Object Repository/Ribbon/MoreCommands/InlineDialog1_Iframe'), 10, FailureHandling.STOP_ON_FAILURE)
			WebUI.waitForElementClickable(findTestObject('Object Repository/Ribbon/MoreCommands/btnAdd2'), 10, FailureHandling.STOP_ON_FAILURE)
			bp.clickJSByElement(findTestObject('Object Repository/Ribbon/MoreCommands/btnAdd2'))
			WebUI.delay(5)

			WebUI.switchToDefaultContent(FailureHandling.STOP_ON_FAILURE)
			bp.clickJSByElement(findTestObject('Object Repository/Ribbon/btnRefresh'))
			WebUI.switchToFrame(findTestObject('Object Repository/New Case/contentIFrame1'), 10, FailureHandling.STOP_ON_FAILURE)
			caseOwnerActual = bp.getValue(findTestObject('Object Repository/New Case/lblCaseOwner'))
			statusActual = bp.getValue(findTestObject('Object Repository/New Case/lblStatus'))

			if(statusActual.toLowerCase().equals("resolved")){
				KeywordUtil.markPassed("Success Resolved the case with case owner is "+caseOwnerActual+" and status is "+statusActual)
				//WebUI.takeScreenshot("Resolved - "+taskSubject)
				WebUI.takeScreenshot(FailureHandling.STOP_ON_FAILURE)
				WebUI.delay(1)
				return true
			}else{
				KeywordUtil.markFailed("Failed Resolved the case because case owner is "+caseOwnerActual+" and status is "+statusActual)
				//WebUI.takeScreenshot("Error - Resolved - "+taskSubject)
				WebUI.takeScreenshot(FailureHandling.STOP_ON_FAILURE)
				WebUI.delay(1)
				return false
			}

		}else{
			KeywordUtil.markFailed("Failed Resolved the case because case owner is "+caseOwnerActual+" and status is "+statusActual)
			//WebUI.takeScreenshot("Error - Resolved - "+taskSubject)
			WebUI.takeScreenshot(FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(1)
			return false
		}
	}

	@Keyword
	public void run(String creator){
		int row = findTestData("workflowReferences").getRowNumbers()
		int clm = findTestData("workflowReferences").getColumnNumbers()

		ArrayList<String> data = new ArrayList<String>();
		data = findTestData("workflowReferences").getAllData()

		if(workflowCode != null){
			for(int i=0; i<row; i++){
				if(data.get(i).get(0).toString().toLowerCase().equals(workflowCode.toLowerCase())){
					for(int j=1; j<clm; j++){
						if(!(data.get(i).get(j).toString().toLowerCase().equals("resolved"))){
							//save and route
							if(!saveAndRoute(data.get(i).get(j), creator)){
								println "Wrong team when save and route"
								KeywordUtil.markFailed("Wrong team when save and route")
								WebUI.verifyMatch(data.get(i).get(j).toString(), caseOwnerActual, true)
								break;
							}
						}else{
							//Resolved
							if(resolved(creator.toString().toLowerCase())){
								WebUI.verifyMatch(creator.toLowerCase(), caseOwnerActual.toLowerCase(), true)
							}else{
								WebUI.verifyMatch(creator.toLowerCase(), caseOwnerActual.toLowerCase(), true)
							}
							break;
						}
					}
					break;
				}else{
					if(i != row-1){
						println "next Workflow iteration "+i
					}else{
						println "Workflow not found"
						KeywordUtil.markErrorAndStop("WorkflowCode not Found")
					}
				}
			}
		}else{
			println "Workflow Code is null"
			KeywordUtil.markErrorAndStop("WorkflowCode Code is null")
		}
	}
}
